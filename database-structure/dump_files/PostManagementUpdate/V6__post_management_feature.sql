-- -----------------------------------------------------
-- Table `post_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `post_categories`
(
    `id`   INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(60)  NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE
)
    ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `projects_participants`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projects_participants`
(
    `participant_id` INT UNSIGNED NOT NULL,
    `project_id`     INT UNSIGNED NOT NULL,
    INDEX `fk_projects_participants_users1_idx` (`participant_id` ASC) VISIBLE,
    INDEX `fk_projects_participants_projects1_idx` (`project_id` ASC) VISIBLE,
    CONSTRAINT `fk_projects_participants_users1`
        FOREIGN KEY (`participant_id`)
            REFERENCES `users` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_projects_participants_projects1`
        FOREIGN KEY (`project_id`)
            REFERENCES `projects` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `post_images`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `post_images`
(
    `id`               INT UNSIGNED       NOT NULL AUTO_INCREMENT,
    `path`             TEXT               NOT NULL,
    `description`      VARCHAR(45)        NULL,
    `main`             ENUM ('YES', 'NO') NOT NULL,
    `posts_details_id` INT UNSIGNED       NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    INDEX `fk_post_images_posts_details1_idx` (`posts_details_id` ASC) VISIBLE,
    CONSTRAINT `fk_post_images_posts_details1`
        FOREIGN KEY (`posts_details_id`)
            REFERENCES `posts_details` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `post_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `post_type`
(
    `id`       INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`     VARCHAR(45)  NOT NULL,
    `posts_id` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    INDEX `fk_post_type_posts1_idx` (`posts_id` ASC) VISIBLE,
    CONSTRAINT `fk_post_type_posts1`
        FOREIGN KEY (`posts_id`)
            REFERENCES `posts` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;