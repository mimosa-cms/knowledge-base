INSERT INTO project_statuses(`value`, `description`)
VALUES ('AVAILABLE', 'Project is available to use'),
       ('BANNED', 'Project is banned, due to not following mimosa policy');