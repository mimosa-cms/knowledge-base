INSERT INTO roles (`id`, `value`)
VALUES (1, 'admin'),
       (2, 'moderator'),
       (3, 'user');
INSERT INTO permissions (`id`, `name`, `description`)
VALUES (1, 'manage_users', 'This permission allows you to manage all the users with user role in the application'),
       (2, 'manage_moderators',
        'This permission allows you to manage all the users with moderator role in the application'),
       (3, 'manage_permissions', 'This permission allows you to manage all the permissions in the application'),
       (4, 'manage_websites', 'This permission allows you to manage users websites'),
       (5, 'manage_roles', 'This permission allows you to modify user roles'),
       (6, 'modify_users', 'This permission allows you to modify users roles'),
       (7, 'create_projects', 'This permission allows you to create projects in the mimosa cms');
INSERT INTO roles_permissions (`role_id`, `permission_id`)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (1, 7),
       (2, 1),
       (2, 4),
       (2, 7),
       (3, 7);
INSERT INTO link_categories (`id`, `name`)
VALUES (1, 'github'),
       (2, 'youtube'),
       (3, 'linkedin'),
       (4, 'facebook'),
       (5, 'twitter'),
       (6, 'instagram'),
       (8, 'reddit'),
       (9, 'spotify'),
       (10, 'tiktok'),
       (11, 'pinterest'),
       (12, 'snapchat'),
       (13, 'gitlab'),
       (14, 'applemusic'),
       (15, 'telegram');
