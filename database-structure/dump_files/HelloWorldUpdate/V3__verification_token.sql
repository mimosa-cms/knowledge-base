CREATE TABLE verification_token
(
    `id`              INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user_id`         INT UNSIGNED NOT NULL,
    `token`           VARCHAR(255) NOT NULL,
    `expiration_date` TIMESTAMP,
    INDEX `fk_verification_token_users_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_verification_token_users`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);