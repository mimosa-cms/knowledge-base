-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mimosa_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users`
(
    `id`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`              VARCHAR(45)  NOT NULL,
    `second_name`       VARCHAR(50)  NOT NULL,
    `email`             VARCHAR(50)  NOT NULL,
    `username`          VARCHAR(45)  NOT NULL,
    `password`          VARCHAR(128) NOT NULL,
    `created_at`        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `is_email_approved` TINYINT(1)   NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
    UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project_statuses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_statuses`
(
    `id`          INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `value`       VARCHAR(45)  NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projects`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projects`
(
    `id`                INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`              VARCHAR(100) NOT NULL,
    `link`              VARCHAR(255) NOT NULL,
    `user_id`           INT UNSIGNED NOT NULL,
    `project_status_id` INT UNSIGNED NOT NULL,
    `created_at`        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    INDEX `fk_projects_users_idx` (`user_id` ASC) VISIBLE,
    INDEX `fk_projects_project_statuses1_idx` (`project_status_id` ASC) VISIBLE,
    UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
    CONSTRAINT `fk_projects_users`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_projects_project_statuses1`
        FOREIGN KEY (`project_status_id`)
            REFERENCES `project_statuses` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `posts_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `posts_details`
(
    `id`               INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `meta_words`       VARCHAR(250) NULL,
    `meta_description` VARCHAR(500) NULL,
    `meta_authors`     VARCHAR(45)  NULL,
    `text`             TEXT         NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `posts`
(
    `id`               INT UNSIGNED                NOT NULL AUTO_INCREMENT,
    `title`            VARCHAR(100)                NOT NULL,
    `description`      VARCHAR(200)                NULL,
    `authors`          VARCHAR(250)                NOT NULL,
    `project_id`       INT UNSIGNED                NOT NULL,
    `posts_details_id` INT UNSIGNED                NOT NULL,
    `article_status`   ENUM ('DRAFT', 'PUBLISHED') NOT NULL DEFAULT 'DRAFT',
    `created_at`       TIMESTAMP                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`       TIMESTAMP                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `likes`            INT UNSIGNED                NOT NULL DEFAULT 0,
    `views`            INT UNSIGNED                NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    INDEX `fk_posts_projects1_idx` (`project_id` ASC) VISIBLE,
    INDEX `fk_posts_posts_details1_idx` (`posts_details_id` ASC) VISIBLE,
    CONSTRAINT `fk_posts_projects1`
        FOREIGN KEY (`project_id`)
            REFERENCES `projects` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_posts_posts_details1`
        FOREIGN KEY (`posts_details_id`)
            REFERENCES `posts_details` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `roles`
(
    `id`    INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `value` VARCHAR(45)  NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    UNIQUE INDEX `value_UNIQUE` (`value` ASC) VISIBLE
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `users_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users_roles`
(
    `user_id` INT UNSIGNED NOT NULL,
    `role_id` INT UNSIGNED NOT NULL,
    INDEX `fk_users_roles_users1_idx` (`user_id` ASC) VISIBLE,
    INDEX `fk_users_roles_roles1_idx` (`role_id` ASC) VISIBLE,
    CONSTRAINT `fk_users_roles_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_users_roles_roles1`
        FOREIGN KEY (`role_id`)
            REFERENCES `roles` (`id`)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `permissions`
(
    `id`          INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`        VARCHAR(50)  NOT NULL,
    `description` VARCHAR(250) NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `roles_permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `roles_permissions`
(
    `role_id`       INT UNSIGNED NOT NULL,
    `permission_id` INT UNSIGNED NOT NULL,
    INDEX `fk_roles_permissions_roles1_idx` (`role_id` ASC) VISIBLE,
    INDEX `fk_roles_permissions_permissions1_idx` (`permission_id` ASC) VISIBLE,
    CONSTRAINT `fk_roles_permissions_roles1`
        FOREIGN KEY (`role_id`)
            REFERENCES `roles` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_roles_permissions_permissions1`
        FOREIGN KEY (`permission_id`)
            REFERENCES `permissions` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `link_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `link_categories`
(
    `id`   INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50)  NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `links`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `links`
(
    `id`               INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`             VARCHAR(50)  NULL,
    `value`            VARCHAR(45)  NOT NULL,
    `post_id`          INT UNSIGNED NOT NULL,
    `link_category_id` INT UNSIGNED NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    INDEX `fk_links_posts1_idx` (`post_id` ASC) VISIBLE,
    INDEX `fk_links_link_categories1_idx` (`link_category_id` ASC) VISIBLE,
    CONSTRAINT `fk_links_posts1`
        FOREIGN KEY (`post_id`)
            REFERENCES `posts` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_links_link_categories1`
        FOREIGN KEY (`link_category_id`)
            REFERENCES `link_categories` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
