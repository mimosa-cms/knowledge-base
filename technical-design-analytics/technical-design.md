# Technical design. Mimosa Analytics
**Here you can find a preliminary technical design of Mimosa Analytics. Disclaimer: this document will changed soon You will notice an absence of schemas and graphics, but wait for them after the first release.**

## Steck of technologies
 - Python as a basic programming language
 - Flask (framework)
 - Libraries: numPy, Pandas, MatplotLib.
 - REST API
 
## Features' attribtes
 - Connect
 - ConnectAPI
 - Validator
 - MissingValues - abc
 - HeirMissingValues - example of an inheriting class
 - CreateResponse
 - Model
 - IdealExample
 - Redirection
