# Basic Technical Design Mimosa CMS
This technical design was build on first project [database structure](./database-structure/database-screenshot.png) for "Hello World!" update, and represent itself as list of classes that will be used for working with objects in some features.
## Permission feature
- PermissionEntity
- Permission
- PermissionsRepository
- PermissionsServiceImpl implements PermissionsService
- UserListController
- AccountController
## Authentication feature
- RoleEntity
- Role
- RoleConstants
- AccountController
- UserListController
- RoleRepository
- RoleServiceImpl implements RoleService
- UserListServiceImpl implements UserListService
- UserListController
- AuthenticationController
- CustomUserDetails extends UserDetails
- CustomUserDetailsService extends UserDetailsService
- WebSecurityConfig
- UserEntity
- User
- UserRepository
- FacebookLoginService
- GoogleLoginService
- TwitterLoginService
## Posts feature
- PostsController
- PostsAPIController
- PostsServiceImpl implements PostsService
- PostsAPIServiceImpl implements PostsAPIService
- Project
- ProjectEntity
- Post
- PostEntity
- ProjectController
- ProjectServiceImpl implements ProjectSerivce
- PostsController
- PostsServiceImpl implements PostsService
- PostsRepository
- ProjectRepository

Technical design may be changed due to the development process in the future.
