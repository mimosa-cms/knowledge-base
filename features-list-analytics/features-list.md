# Features List and Release Plan of Mimosa Analytics

## Features
 - The server's settings
 - The main API requests/responses
 - The data base connections
 - The filtering of data
 - The missing values' fight
 - Creation of the regular response. JSON
 - End-points
 - Anti Injection's validation
 - Ideal example
 - Creation of a learning model
 
*** Release Plan ***
## "Set up your environment" - the first release
 - The server's settings
 - The main API requests/responses
 - The data base connections
 - The filtering of data
## "Make it work" - the second release
 - The missing values' fight
 - Creation of the regular response. JSON
 - End-points
## "Modernize an app"
 - Anti Injection's validation
 - Ideal example
 - Creation of learning model
