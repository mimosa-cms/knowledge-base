# Feature and Releases | Mimosa CMS
All functional requirements numbers was taken from the [functional requiremnets document](./functional-requirements/functional-requirements.md)
## Releases and features
### Release number 1 - "Hello World! update"
Number of functional requirements that included in the release: `1, 2, 3, 4, 5, 6, 8, 9, 10, 13, 17, 20`
>- Project Init
>- Front-end set up (1, 17)
>- Permissions feature (3)
>- Authentication feature (2, 4, 5)
>- Posts feature (10, 20, 30)
### Release number 2 - "Posts managment update"
Number of functional requirements that included in the release: `19, 21, 22, 25`
>- Posts managment feature (19)
>- Big text box feature (21)
>- Images feature (22)
>- Coworkers feature (25)
### Release number 3 - "Security update"
Number of functional requirements that included in the release: `7, 14, 15, 16`
>- Policy feature (7)
>- 3fa feature (13, 14, 15, 16)
>- Style check (32)
### Release number 4 - "Analytics update"
Number of functional requirements that included in the release: `6, 8, 9, 11, 12, 18, 20, 28`
>- Analytics feature (11, 12)
>- CMS API (6, 8, 9, 20)
>- CDN connect feature (28)
>- Documentation feature (18)
### Release number 5 - "Usability update"
Number of functional requirements that included in the release: `23, 24, 26, 27`
>- feedback feature (23)
>- donate feature (24, 31, 32)
>- academy feature (26)
>- Admin panel support for Admins, and moderators (27)
>- scheduling posts feature (29)

### References
All dedlines will be presented in our blog.
If you have new features ideas to share - write to me via our blog.

If you have your own ideas fill free to copy the main application repository and code your feature. After that you can create merge request. Be sure, that your code convections and styles are the same that we have.
