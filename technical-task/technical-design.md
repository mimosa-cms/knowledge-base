# Mimosa CMS technical task

Mimosa CMS is a high-functional product that requires trouble-free integration with user sites and
it is easy to use (create new posts).

The following CMS were considered as competitors and the basis of this project:

1. https://boltcms.io/
2. https://voyager.devdojo.com/
3. https://github.com/buttasam/cms-boot

Criteria and functionality that must be included in the system:

- CMS as a separate application, and not a ready-made project skeleton for the client
- Ease to download/integrate
- Easily customisable and user-friendly system
- The main resistance on posts created in the program and their functionality and management
- Easy API usage
- Available built-in analytics
- The aptitude for powerful data protection
- The system should be free and open-source
- CMS administrators and moderators should be in might to monitor sites (projects) registered
    by users and remove them in case of disturbing our policy or norms of intelligence.The client
    will be able to create only 3 projects
- The capability for customers to leave feedback.

Design wishes:

- Bootstrap 5 version
- For corporate site use something like: https://startbootstrap.com/theme/landing-page
- Fonts and theme origin webbrains
- Mimosa CMS dashboard use template like this: https://bootstraptema.ru/stuff/
    templates_bootstrap/admin/dash_ui/4-1-0-