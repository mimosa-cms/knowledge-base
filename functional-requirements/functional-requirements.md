# Mimosa CMS Functional Requirements
1. Project ought to have a corporative part (like introduction to the CMS, landing page with the explanation of the project) and system part (working space of the CMS).
2. Project must have three roles of registered users: Admin, moderator and user.
3. Roles must have their customizable permissions (For example if users did not configure API key - users won't be able to access API).
4. Admin can see all users and moderators of the application and can control other types of accounts (delete accounts when it is necessary, delete websites if there is a strong reason, create new moderators or promote regular users).
5. Moderators can controll websites that users are making.
6. Posts can be accessed via API.
7. Website needs to have own policy (reasons why users can be banned or deleted) + support of banning account due to not following the policy (moderators and admins).
8. Users can access pictures via API.
9. API is accessed by created API key.
10. Users can register only 3 applications and can have only 3 projects un free account.
11. Users can request analytics.
12. Users can export analytics to PDF file.
13. Users can register account via Google or apple or Twitter or Facebook or native authorization with email approval.
14. Users can turn on or off 2-fa authorization.
15. Users can have several 2-fa types (change between them).
16. 2-fa can be two types: sending code to email and via Google authenticator application.
17. Project design have to be easy to use and adaptive.
18. API has to have a (built-in) documentation with examples on `Angular or React` on a website and offer examples on `Angular or React`.
19. User can create their own category and types of posts (like type - youtube content and category - games) or choose category from prepared and filter that.
20. Pagination system to posts included (even in API).
21. Users needs to have easy redactor to create `html` based text for posts.
22. Users can have avatar and add images to their websites or posts.
23. Application needs to have a feedback form.
24. Application needs to have a donate section (PayPal), when project will be open-source. And subscription system, when project will be not open-source.
25. Mimosa must allow adding colleagues (coworkers) in user's personal projects.
26. (Optional) - create academy website section, like voyager, it might explain users how to use CMS.
27. Add admin panel support for project maintainance [admin panel](https://github.com/aileftech/snap-admin)
28. Posts can be published, accessed via any of the listed social media (facebook, instagram, telegram etc.).
29. Possibility to schedule the post publication.
30. Post length is 4096 for free trier.
31. Post length is 10 000 for subscribers.
32. Checkstyle plugin should be added and configured for the project.
