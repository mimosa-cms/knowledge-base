# Mimosa CMS knowladge base

### Documents plan for CMS part:
> 1. Technical task
> 2. Functional requirements
> 3. Technical design
> 4. Database structure
> 5. Feature and release plan
> 6. API documentation
### Documents plan for analytics application part:
> 1. Functional requirements
> 2. Technical design
> 3. Features and release plan
> 4. API documentation

## Documents
Documents for CMS application:
- [Teachnical task](./technical-task/technical-task.pdf)
- [Functional requirements](./functional-requirements/functional-requirements.md)
- [Feature list](./feature-list/feature-list.md)
- [CMS usage policy](./policy/Acceptable policy of Mimosa.pdf)
- [Database structure](./database-structure/database-screenshot.png)
- [Technical design](./technical-design/technical-design.md)