# Mimosa Analytics functional requirements
## Mimosa analytics system was created as an essential part of the main project - Mimosa CMS. Nevertheless, Mimosa Analytics is an independent product and it is usable for a wide range of applications.

1. The project must be API-based. The system communicates with clients part via end-points.
2. Development is going on Python.
3. Mimosa Analytics is responsible for selecting required by user parameters from data bases and processing acquired data into logical and clear fields, making them ready for graphs creation.
4. In the preprocessing stage, data has to rid of missing values. In case of enormous occasions of missing values(more than 30% of rows), that leads to distortion of results, all numerical fields should be changed with no empty element of the subsequence of the previous row's value; all categorical data should be changed on the most frequently happened item in a column or inserted due to percentage of types (ex. In a sheet you have such a distribution of boolean data: 30% - True, 70% - False. So that is a formula for switching missing values); objects with empty personal information preferably must be deleted if it is important for further processes. 
5. As a part of regular response, in most cases, the system returns average, most and less usable variables.
6. Optional, prediction of aimed auditory is wanted. Application might create an ideal candidate. When a client pushes some parameters, Mimosa Analytics answers whether it is an  appropriate candidate or not.
